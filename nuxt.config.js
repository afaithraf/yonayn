export default {
    modules: ['@nuxt/content'],
    css: [
        'normalize.css',
        '~/assets/css/main.scss'
    ],
    target: 'static',
    components: true,
    plugins: [{
        src: '@/plugins/plugin',
        mode: 'client'
    }],
    // global head 
    head: {
        htmlAttrs: {
          lang: 'en'
        },
        title: 'Yonayn | We Build Websites that IMPRESS',
        meta: [{
                charset: 'utf-8'
            },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            },
            {
                hid: 'description',
                name: 'description',
                content: 'Hire the best web developers and web designers, to build your website now. We are a web design/development studio, we focus on building elegant websites for successful businesses.'
            }
        ],
        link: [{
                rel: 'icon',
                type: 'image/x-icon',
                href: '/favicon.ico' // favicon
            },
            {
                rel: 'stylesheet',
                href: 'https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&display=swap' // poppins font
            }
        ]
    },


    loading: {
        color: '#ffc938'
    }

}