import Vue from "vue";

//unicons
import Unicon from "vue-unicons/dist/vue-unicons-ssr.common.js";
import { uniBars, uniAngleUp, uniCheck } from "vue-unicons/src/icons";
import "vue-unicons/dist/vue-unicons-ssr.css";
Unicon.add([uniBars, uniAngleUp, uniCheck]);
Vue.use(Unicon);

//v-select
import vSelect from "vue-select";
Vue.component("v-select", vSelect);

import PrettyInput from "pretty-checkbox-vue/input";
import PrettyCheck from "pretty-checkbox-vue/check";
import PrettyRadio from "pretty-checkbox-vue/radio";

Vue.component("p-input", PrettyInput);
Vue.component("p-check", PrettyCheck);
Vue.component("p-radio", PrettyRadio);
